// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Progect.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TPavlovaSveta *PavlovaSveta;

// ---------------------------------------------------------------------------
__fastcall TPavlovaSveta::TPavlovaSveta(TComponent* Owner) : TForm(Owner) {
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::FormResize(TObject *Sender) {
	FloatAnimation1->StopValue = this->Width - imgMenu->Width;
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::startBuMouseEnter(TObject *Sender) {

	TButton *x = ((TButton*) Sender);
	x->Margins->Rect = TRect(0, 0, 0, 0);
	x->TextSettings->Font->Size += 5;
	x->TextSettings->Font->Style =
		x->TextSettings->Font->Style << TFontStyle::fsBold;
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::startBuMouseLeave(TObject *Sender) {

	TButton *x = ((TButton*) Sender);
	x->Margins->Rect = TRect(5, 5, 5, 5);
	x->TextSettings->Font->Size -= 5;
	x->TextSettings->Font->Style =
		x->TextSettings->Font->Style >> TFontStyle::fsBold;
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::FormCreate(TObject *Sender) {
	tc->First();

}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::startBuClick(TObject *Sender) {
	Belka->Visible = false;
	tc->Next();

}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::ButtonAllClick(TObject *Sender) {
	if (((TControl*)Sender)->Tag == 1) {
		FCountCinema++;
		if (sum2 < FCountCinema) {
			y = k;
			sum2 = FCountCinema;
		};
	}
	if (((TControl*)Sender)->Tag == 2) {
		FCountJp++;
		if (sum3 < FCountJp) {
			x = j;
			sum3 = FCountJp;
		};
	}
	if (((TControl*)Sender)->Tag == 3) {
		Belka->Visible = true;
		FCountParkYes++;
		if (sum3 < FCountParkYes) {
			x = yes;
			sum3 = FCountParkYes;
		};
	}
	if (((TControl*)Sender)->Tag == 4) {
		FCountMuseum++;
		if (sum2 < FCountMuseum) {
			y = m;
			sum2 = FCountMuseum;
		};
	}
	if (((TControl*)Sender)->Tag == 5) {
		FCountIt++;
		if (sum3 < FCountIt) {
			x = i;
			sum3 = FCountIt;
		};
	}
	if (((TControl*)Sender)->Tag == 6) {
		FCountParkNo++;
		if (sum3 < FCountParkNo) {
			x = n;
			sum3 = FCountParkNo;
		};
	}
	if (((TControl*)Sender)->Tag == 125) {
		FCountCinema++;
		FCountJp++;
		FCountIt++;
		if (sum2 < FCountCinema) {
			y = k;
			sum2 = FCountCinema;
		};
		if (sum3 < FCountJp) {
			x = j;
			sum3 = FCountJp;
		};
		if (sum3 < FCountIt) {
			x = i;
			sum3 = FCountIt;
		};
	}
	if (((TControl*)Sender)->Tag == 346) {
		FCountParkYes++;
		FCountMuseum++;
		FCountParkNo++;
		if (sum3 < FCountParkYes) {
			x = yes;
			sum3 = FCountParkYes;
		};
		if (sum2 < FCountMuseum) {
			y = m;
			sum2 = FCountMuseum;
		};
		if (sum3 < FCountParkNo) {
			x = n;
			sum3 = FCountParkNo;
		};
	}
	if (((TControl*)Sender)->Tag == 25) {
		FCountJp++, FCountIt++;
		if (sum3 < FCountJp) {
			x = j;
			sum3 = FCountJp;
		};
		if (sum3 < FCountIt) {
			x = i;
			sum3 = FCountIt;
		};
	}
	if (((TControl*)Sender)->Tag == 36) {
		FCountParkYes++, FCountParkNo++;
	}
	if (sum3 < FCountParkYes) {
		x = yes;
		sum3 = FCountParkYes;
	};
	if (sum3 < FCountParkNo) {
		x = n;
		sum3 = FCountParkNo;
	};
	tc->Next();
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::buRestartClick(TObject *Sender) {
	FCountJp = 0;
	FCountIt = 0;
	FCountParkYes = 0;
	FCountParkNo = 0;
	FCountMuseum = 0;
	FCountCinema = 0;
    memo->Lines->Clear();
	tc->First();
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::buEndClick(TObject *Sender) {
	memo->Lines->Add("����� ����� � " + x);
	memo->Lines->Add("��� �� ����� ����� � " + y);
	tc->Next();
}
// ---------------------------------------------------------------------------

void __fastcall TPavlovaSveta::buRandClick(TObject *Sender)
{
	int d, rnd;
	String S;
	S = "";
	for (d = 0; d < 1; d++) {
	  rnd = Random(6);
	  S += place[rnd];
	}
	memo->Lines->Add("����� ����� � " + S);
	tc->ActiveTab = tiResult;
}
//---------------------------------------------------------------------------

