//---------------------------------------------------------------------------

#ifndef ProgectH
#define ProgectH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TPavlovaSveta : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TButton *startBu;
	TImage *imgMenu;
	TFloatAnimation *FloatAnimation1;
	TLabel *Label1;
	TTabItem *TabItem2;
	TLabel *Label3;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TTabItem *TabItem3;
	TTabItem *TabItem5;
	TLabel *Label4;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button7;
	TButton *Button8;
	TTabItem *TabItem6;
	TGridPanelLayout *GridPanelLayout5;
	TButton *Button9;
	TTabItem *tiResult;
	TButton *Button10;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button11;
	TButton *Button12;
	TLabel *Label6;
	TLabel *Label2;
	TButton *buRestart;
	TMemo *memo;
	TTabItem *tiPreResult;
	TButton *buEnd;
	TColorAnimation *ColorAnimation1;
	TImage *Image1;
	TFloatAnimation *FloatAnimation2;
	TButton *buRand;
	TImage *Belka;
	void __fastcall FormResize(TObject *Sender);
	void __fastcall startBuMouseEnter(TObject *Sender);
	void __fastcall startBuMouseLeave(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall startBuClick(TObject *Sender);
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buEndClick(TObject *Sender);
	void __fastcall buRandClick(TObject *Sender);
private:	// User declarations
int FCountJp=0;
int FCountIt=0;
int FCountParkYes=0;
int FCountParkNo=0;
int FCountMuseum=0;
int FCountCinema=0;
int sum3=0;
int sum2=0;
UnicodeString x;
UnicodeString y;
UnicodeString k = "����. ������ ����:������, �����������: ��������� ����, �����, �������.";
UnicodeString j = "�������� ����. ����� ����� � �����, ���� ���.";
UnicodeString yes = "�����, ��� ���� �������. ��� ���� � ����� ��������, �������� � � ����� ������";
UnicodeString m = "�����. �������� � ������, ����, ����� ��������.";
UnicodeString i = "���� � ����������� ������. ��� ���� �����, ������� �����.";
UnicodeString n = "�����, ��� ��� �������. ����� ��� ����, ���� ���������, ��������������� ���, ��������� ����.";
String place[6] = {k,j,yes,m,i,n};
public:		// User declarations
	__fastcall TPavlovaSveta(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPavlovaSveta *PavlovaSveta;
//---------------------------------------------------------------------------
#endif
