object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 369
  Width = 510
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Users\Svetlana\Desktop\DB\'#1057#1055#1048#1057#1054#1050' '#1055#1056#1054#1044#1059#1050#1058#1054#1042'.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB'
      'DropDatabase=No')
    FormatOptions.AssignedValues = [fvSE2Null]
    FormatOptions.StrsEmpty2Null = True
    Connected = True
    LoginPrompt = False
    Left = 192
    Top = 48
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 320
    Top = 280
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 440
    Top = 296
  end
  object FDQList: TFDQuery
    DetailFields = 'NAME;OTHER;QUANTITY'
    Connection = FDConnection1
    UpdateOptions.AssignedValues = [uvGeneratorName]
    UpdateOptions.GeneratorName = 'ID_LIST'
    UpdateOptions.UpdateTableName = '"LIST"'
    SQL.Strings = (
      'select '
      '    list.id,'
      '    list.name,'
      '    list.quantity,'
      '    list.other'
      'from list')
    Left = 136
    Top = 152
    object FDQListNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 50
    end
    object FDQListQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
    end
    object FDQListOTHER: TWideStringField
      FieldName = 'OTHER'
      Origin = 'OTHER'
      Size = 50
    end
    object FDQListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object FDQLongList: TFDQuery
    DetailFields = 'NAME;OTHER;PRICE'
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    "LONG LIST".name,'
      '    "LONG LIST".price,'
      '    "LONG LIST".other,'
      '    "LONG LIST".id_long'
      'from "LONG LIST"')
    Left = 200
    Top = 152
    object FDQLongListNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 50
    end
    object FDQLongListPRICE: TIntegerField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
    object FDQLongListOTHER: TWideStringField
      FieldName = 'OTHER'
      Origin = 'OTHER'
      Size = 50
    end
    object FDQLongListID_LONG: TIntegerField
      FieldName = 'ID_LONG'
      Origin = 'ID_LONG'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
end
