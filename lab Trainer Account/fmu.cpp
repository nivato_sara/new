//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::DoReset() {
  int FCountCorrect=0;
  int FCountWrong=0;
  DoContinue();
  DoTimer();
}
 //---------------------------------------------------------------------------
void TForm1::DoTimer()  {
  	FTimeStart=Now();
	Timer1->Enabled=true ;
}
//---------------------------------------------------------------------------
void TForm1::DoContinue() {
	laCorrect->Text=Format(L"True= %d", ARRAYOFCONST((FCountCorrect)));
	laWrong->Text=Format(L"False= %d", ARRAYOFCONST((FCountWrong)));

	int xValue1=Random(20);
	int xValue2=Random(20);
	int xSign=(Random(2)==1)?1:-1;
	int xResult=xValue1+xValue2;
	int xResultNew=(Random(2)==1)? xResult:xResult+(Random(7)*xSign);

	FAnswerCorrect=(xResult==xResultNew);
	laCode->Text=Format("%d+%d=%d",
	ARRAYOFCONST((xValue1,xValue2,xResultNew)));


}
//---------------------------------------------------------------------------
void TForm1::DoAnswer(bool aValue)  {
	(aValue==FAnswerCorrect)? FCountCorrect++ : FCountWrong++;
	DoContinue();
}

//---------------------------------------------------------------------------
void __fastcall TForm1::buRestartClick(TObject *Sender)
{    Randomize();
	DoReset();

}
//---------------------------------------------------------------------------
void __fastcall TForm1::byYesClick(TObject *Sender)
{
	 DoAnswer(true);
	 DoTimer();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::byNoClick(TObject *Sender)
{
	 DoAnswer(false);
	 DoTimer();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
	DoReset();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::byAboutClick(TObject *Sender)
{
	ShowMessage(L"Pavlova Sveta 171-334");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
UnicodeString x;
	DateTimeToString(x, L"h:nn:ss.zzz", Now()-FTimeStart);
	laTime ->Text = x.Delete(x.Length() -2, 2);
}
//---------------------------------------------------------------------------




