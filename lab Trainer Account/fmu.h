//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buRestart;
	TButton *byAbout;
	TGridPanelLayout *GridPanelLayout1;
	TButton *byYes;
	TButton *byNo;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TLabel *laCorrect;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *Label3;
	TLabel *laCode;
	TTimer *Timer1;
	TLabel *laTime;
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall byYesClick(TObject *Sender);
	void __fastcall byNoClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall byAboutClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	bool FAnswerCorrect;
	void DoReset();
	void DoContinue();
	void DoAnswer(bool aValue);
	TDateTime FTimeStart;
	void  DoTimer();

public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
