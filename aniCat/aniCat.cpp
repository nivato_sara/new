//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "aniCat.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	FCountCat = 0;
	FCountRaccon = 0;

}

//---------------------------------------------------------------------------
void __fastcall TForm1::FormResize(TObject *Sender)
{
	FloatAnimation1->StopValue = this->Width-imCat->Width;
	FloatAnimation2->StopValue = this->Width-imRaccon->Width;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::imCatClick(TObject *Sender)
{
	FCountCat++;
	laCountCat->Text="Cat = "+ IntToStr(FCountCat);
	FloatAnimationCat->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::imRacconClick(TObject *Sender)
{
	FCountRaccon++;
	laCountRaccon->Text="Raccon = "+ IntToStr(FCountRaccon);
	FloatAnimationRaccon->Start();
}
//---------------------------------------------------------------------------
