//---------------------------------------------------------------------------

#ifndef aniCatH
#define aniCatH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLabel *laCountCat;
	TLabel *laCountRaccon;
	TImage *imCat;
	TImage *imRaccon;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TFloatAnimation *FloatAnimationCat;
	TFloatAnimation *FloatAnimationRaccon;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall imCatClick(TObject *Sender);
	void __fastcall imRacconClick(TObject *Sender);
	void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall FloatAnimationCatFinish(TObject *Sender);
	void __fastcall imCatResized(TObject *Sender);
	void __fastcall FloatAnimation2Finish(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
private:	// User declarations
	int FCountCat;
    int FCountRaccon;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
