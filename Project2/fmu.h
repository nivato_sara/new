// ---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>

// ---------------------------------------------------------------------------
class Tfm : public TForm {
__published: // IDE-managed Components
	TLayout *Layout1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TButton *startBu;
	TImage *imgMenu;
	TFloatAnimation *FloatAnimation1;
	TLabel *Label1;
	TTabItem *ti1;
	TLabel *Label3;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TTabItem *ti2;
	TTabItem *ti3;
	TLabel *Label4;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button7;
	TButton *Button8;
	TTabItem *ti4;
	TGridPanelLayout *GridPanelLayout5;
	TButton *Button9;
	TButton *Button10;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button11;
	TButton *Button12;
	TLabel *Label6;
	TLabel *Label2;
	TTabItem *tiPreResult;
	TButton *buEnd;
	TColorAnimation *ColorAnimation1;
	TImage *Image1;
	TFloatAnimation *FloatAnimation2;
	TButton *buRand;
	TImage *Belka;
	TTabItem *tiend;
	TBindSourceDB *BSPlace;
	TListView *ListView1;
	TBindingsList *BindingsList1;
	TLabel *Label5;
	TButton *Button6;
	TBindSourceDB *BSPlace2;
	TImage *Image2;
	TListView *ListView2;
	TLinkFillControlToField *LinkFillControlToField1;
	TListView *ListView3;
	TLinkFillControlToField *LinkFillControlToField2;
	TButton *buRand2;

	void __fastcall FormResize(TObject *Sender);
	void __fastcall startBuMouseEnter(TObject *Sender);
	void __fastcall startBuMouseLeave(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall startBuClick(TObject *Sender);
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buEndClick(TObject *Sender);
	void __fastcall buRandClick(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);

private: // User declarations
		int FCountJp = 0;
	int FCountIt = 0;
	int FCountParkYes = 0;
	int FCountParkNo = 0;
	int FCountMuseum = 0;
	int FCountCinema = 0;
	int sum3 = 0;
	int sum2 = 0;

	UnicodeString x;
	UnicodeString y;

	UnicodeString k =
		"select p.place_name, p.name,  p.image from place p where ( place_name = 'Movie')";
	UnicodeString j =
		"select p.place_name, p.name,  p.image from place p where ( other = 'Japanese')";
	UnicodeString yes =
		"select p.place_name, p.name,  p.image from place p where ( other = 'with squirrels')";
	UnicodeString m =
		"select p.place_name, p.name,  p.image from place p where ( place_name = 'Museum')";
	UnicodeString i =
		"select p.place_name, p.name,  p.image from place p where ( other = 'Italian')";
	UnicodeString n =
		"select p.place_name, p.name,  p.image from place p where ( other = 'without squirrels')";
	String place[6] = {k, j, yes, m, i, n};

public: // User declarations
	__fastcall Tfm(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
// ---------------------------------------------------------------------------
#endif
