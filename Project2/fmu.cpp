// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "PavlovaSvetaProgect1PCH1.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;

// ---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner) : TForm(Owner) {
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::FormResize(TObject *Sender) {
	FloatAnimation1->StopValue = this->Width - imgMenu->Width;
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::startBuMouseEnter(TObject *Sender) {

	TButton *x = ((TButton*) Sender);
	x->Margins->Rect = TRect(0, 0, 0, 0);
	x->TextSettings->Font->Size += 5;
	x->TextSettings->Font->Style =
		x->TextSettings->Font->Style << TFontStyle::fsBold;
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::startBuMouseLeave(TObject *Sender) {

	TButton *x = ((TButton*) Sender);
	x->Margins->Rect = TRect(5, 5, 5, 5);
	x->TextSettings->Font->Size -= 5;
	x->TextSettings->Font->Style =
		x->TextSettings->Font->Style >> TFontStyle::fsBold;
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender) {
	tc->First();
//	dm->FDConnection1->Open();
//	dm->qu->Active = True;
//	dm->qu2->Active = True;
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::startBuClick(TObject *Sender) {
	Belka->Visible = false;
	tc->Next();
	dm->qu->SQL->Clear();
	dm->qu2->SQL->Clear();
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::ButtonAllClick(TObject *Sender) {
	if (((TControl*)Sender)->Tag == 1) {
		FCountCinema++;
		if (sum2 < FCountCinema) {
			y = k;
			sum2 = FCountCinema;
		};
	}
	if (((TControl*)Sender)->Tag == 2) {
		FCountJp++;
		if (sum3 < FCountJp) {
			x = j;
			sum3 = FCountJp;
		};
	}
	if (((TControl*)Sender)->Tag == 3) {
		Belka->Visible = true;
		FCountParkYes++;
		if (sum3 < FCountParkYes) {
			x = yes;
			sum3 = FCountParkYes;
		};
	}
	if (((TControl*)Sender)->Tag == 4) {
		FCountMuseum++;
		if (sum2 < FCountMuseum) {
			y = m;
			sum2 = FCountMuseum;
		};
	}
	if (((TControl*)Sender)->Tag == 5) {
		FCountIt++;
		if (sum3 < FCountIt) {
			x = i;
			sum3 = FCountIt;
		};
	}
	if (((TControl*)Sender)->Tag == 6) {
		FCountParkNo++;
		if (sum3 < FCountParkNo) {
			x = n;
			sum3 = FCountParkNo;
		};
	}
	if (((TControl*)Sender)->Tag == 125) {
		FCountCinema++;
		FCountJp++;
		FCountIt++;
		if (sum2 < FCountCinema) {
			y = k;
			sum2 = FCountCinema;
		};
		if (sum3 < FCountJp) {
			x = j;
			sum3 = FCountJp;
		};
		if (sum3 < FCountIt) {
			x = i;
			sum3 = FCountIt;
		};
	}
	if (((TControl*)Sender)->Tag == 346) {
		FCountParkYes++;
		FCountMuseum++;
		FCountParkNo++;
		if (sum3 < FCountParkYes) {
			x = yes;
			sum3 = FCountParkYes;
		};
		if (sum2 < FCountMuseum) {
			y = m;
			sum2 = FCountMuseum;
		};
		if (sum3 < FCountParkNo) {
			x = n;
			sum3 = FCountParkNo;
		};
	}
	if (((TControl*)Sender)->Tag == 25) {
		FCountJp++, FCountIt++;
		if (sum3 < FCountJp) {
			x = j;
			sum3 = FCountJp;
		};
		if (sum3 < FCountIt) {
			x = i;
			sum3 = FCountIt;
		};
	}
	if (((TControl*)Sender)->Tag == 36) {
		FCountParkYes++, FCountParkNo++;
	}
	if (sum3 < FCountParkYes) {
		x = yes;
		sum3 = FCountParkYes;
	};
	if (sum3 < FCountParkNo) {
		x = n;
		sum3 = FCountParkNo;
	};
	tc->Next();
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buRestartClick(TObject *Sender) {

	FCountJp = 0;
	FCountIt = 0;
	FCountParkYes = 0;
	FCountParkNo = 0;
	FCountMuseum = 0;
	FCountCinema = 0;
	tc->First();
	dm->qu->SQL->Clear();
	dm->qu2->SQL->Clear();

}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buEndClick(TObject *Sender) {
	dm->qu->Active = True;
	dm->qu2->Active = True;
	dm->qu->SQL->Add(x);
	dm->qu2->SQL->Add(y);
	// memo->Lines->Add("����� ����� � " + x);
	// memo->Lines->Add("��� �� ����� ����� � " + y);
	tc->Next();
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buRandClick(TObject *Sender) {
    dm->FDConnection1->Open();
	dm->qu->SQL->Clear();
	dm->qu2->SQL->Clear();
	int d, d1, rnd;
	String S;
	String S1;
	S = "";
	S1 = "";
	for (d = 0; d < 1; d++) {
		rnd = Random(6);
		S += place[rnd];
	}
	for (d1 = 0; d1 < 1; d1++) {
		rnd = Random(6);
		S1 += place[rnd];
		if (S == S1) {
			d1--;
		};
	}

	dm->qu->SQL->Add(S);
	dm->qu2->SQL->Add(S1);
	dm->qu->Active = True;
	dm->qu2->Active = True;
	tc->ActiveTab = tiend;
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender) {
	tc->Next();
	// dm->FDConnection1->Open();
	// dm->qu->SQL->Clear();
	// dm->qu->SQL->Add("select p.place_name, p.name, p.other, p.image, p.id from place p where ( place_name = 'Movie')");
	// dm->qu->Active=True;
}
// ---------------------------------------------------------------------------
