//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labStopWatch.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	FTimeStart=Now();
	tm -> Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStopClick(TObject *Sender)
{
	tm -> Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmTimer(TObject *Sender)
{
	UnicodeString x;
	DateTimeToString(x, L"h:nn:ss.zzz", Now()-FTimeStart);
	laTime ->Text = x.Delete(x.Length() -2, 2);
	//laTime -> Text= TimeToStr(Now()- FTimeStart);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::KrugClick(TObject *Sender)
{
	Memo1 ->Lines-> Add(laTime->Text);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::CleanClick(TObject *Sender)
{
	Memo1->Lines->Clear();
}
//---------------------------------------------------------------------------
