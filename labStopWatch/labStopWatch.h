//---------------------------------------------------------------------------

#ifndef labStopWatchH
#define labStopWatchH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLabel *laTime;
	TButton *buStart;
	TButton *buStop;
	TTimer *tm;
	TButton *Krug;
	TMemo *Memo1;
	TButton *Clean;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall KrugClick(TObject *Sender);
	void __fastcall CleanClick(TObject *Sender);
private:	// User declarations
    TDateTime FTimeStart;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
