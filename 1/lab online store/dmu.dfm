object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 288
  Width = 352
  object FDConnection: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Database=C:\Users\VD\Desktop\lab online store\BD\ONLINESTORE.FDB'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    AfterConnect = FDConnectionAfterConnect
    BeforeConnect = FDConnectionBeforeConnect
    Left = 80
    Top = 72
  end
  object quCategory: TFDQuery
    Active = True
    AfterScroll = quCategoryAfterScroll
    Connection = FDConnection
    SQL.Strings = (
      'select '
      '    *'
      'from category c'
      'order by c.sort_index')
    Left = 72
    Top = 152
    object quCategoryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quCategoryIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 200
    Top = 72
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 216
    Top = 144
  end
  object spFeedbackIns: TFDStoredProc
    Filtered = True
    Connection = FDConnection
    StoredProcName = 'FEEDBACK_INS'
    Left = 216
    Top = 224
    ParamData = <
      item
        Position = 1
        Name = 'FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end>
  end
  object quProduct: TFDQuery
    Active = True
    Connection = FDConnection
    SQL.Strings = (
      'select '
      '    p.id,'
      '    p.category_id,'
      '    p.name,'
      '    p.price,'
      '    p.note,'
      '    p.image'
      'from product p')
    Left = 72
    Top = 224
    object quProductID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProductCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quProductNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quProductPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object quProductNOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object quProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
  end
  object quProductItem: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select '
      '    p.name,'
      '    p.price,'
      '    p.note,'
      '    p.image,'
      '    p.category_id,'
      '    p.id'
      'from product p'
      'order by p.id')
    Left = 160
    Top = 128
  end
end
