//---------------------------------------------------------------------------

#ifndef fruCategoryH
#define fruCategoryH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class TfrCategory : public TFrame
{
__published:	// IDE-managed Components
	TImage *im;
	TLabel *la;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText;
private:	// User declarations
public:		// User declarations
	__fastcall TfrCategory(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrCategory *frCategory;
//---------------------------------------------------------------------------
#endif
