//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "labOnlineStore.h"
#include "fruCategory.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "fruCategory"
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::ReloadCategoryList()
{
dm->quCategory->First();
	while(!dm->quCategory->Eof){
		TfrCategory *x =new TfrCategory(frCategory);
		x->Parent=frCategory;
		x->Align=TAlignLayout::Client;
		x->Name="frCategory" +IntToStr(dm->quCategoryID->Value);
		x->la->Text=dm->quCategoryNAME->Value;
		x->im->Bitmap->Assign(dm->quCategoryIMAGE);
		x->Tag=dm->quCategoryID->Value;
		x->OnClick=buCategoryClick;
		dm->quCategory->Next();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buSendClick(TObject *Sender)
{
dm->FeedbackIns(
edFeedbackFIO->Text,
edFeedbackPhone->Text,
edFeedbackEmail->Text,
meFeedbackNote->Text
);
ShowMessage("����� ���������");
tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::buFeedBackClick(TObject *Sender)
{
tc->GotoVisibleTab(tiFeedBack->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
tc->ActiveTab=tiMenu;
tc->TabPosition=TTabPosition::None;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormShow(TObject *Sender)
{
ReloadCategoryList();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buCategoryClick(TObject *Sender)
{
//////
}
//---------------------------------------------------------------------------

void __fastcall TForm1::frCategoryimClick(TObject *Sender)
{
int xCategoryID=((TControl*)Sender)->Tag;
TLocateOptions xLO;
dm->quCategory->Locate(dm->quCategoryID->FieldName,xCategoryID,xLO);
tc->GotoVisibleTab(tiProductList->Index);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::buCardProductClick(TObject *Sender)
{
 tc->GotoVisibleTab(tiCategory->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
 tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
tc->GotoVisibleTab(tiProductList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
tc->GotoVisibleTab(tiProductList->Index);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::lvProductChange(TObject *Sender)
{
tc->GotoVisibleTab(tiProductItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
tc->GotoVisibleTab(tiCategory->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ListView1Change(TObject *Sender)
{
tc->GotoVisibleTab(tiProductList->Index);
}
//---------------------------------------------------------------------------

