//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include "fruCategory.h"
#include <FMX.Objects.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiProductItem;
	TTabItem *tiProductList;
	TTabItem *tiCategory;
	TTabItem *TabItem5;
	TTabItem *TabItem6;
	TTabItem *TabItem7;
	TTabItem *tiFeedBack;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buCardProduct;
	TButton *Button12;
	TButton *buFeedBack;
	TButton *buCategory;
	TLabel *Label6;
	TScrollBox *ScrollBox1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TEdit *edFeedbackPhone;
	TEdit *edFeedbackFIO;
	TEdit *edFeedbackEmail;
	TMemo *meFeedbackNote;
	TButton *buSend;
	TfrCategory *frCategory;
	TLayout *Layout1;
	TButton *Button1;
	TLabel *Label7;
	TListView *lvProduct;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLayout *Layout2;
	TButton *Button2;
	TButton *Button3;
	TLayout *Layout3;
	TLabel *Label8;
	TBindSourceDB *BindSourceDB2;
	TBindSourceDB *BindSourceDB1;
	TListView *ListView1;
	TLinkListControlToField *LinkListControlToField2;
	TButton *Button4;
	TButton *Button5;
	TLayout *Layout4;
	TButton *Button6;
	TLabel *Label9;
	void __fastcall buSendClick(TObject *Sender);
	void __fastcall buFeedBackClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall buCategoryClick(TObject *Sender);
	void __fastcall frCategoryimClick(TObject *Sender);
	void __fastcall buCardProductClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall lvProductChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ListView1Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
	void  ReloadCategoryList();

	};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
