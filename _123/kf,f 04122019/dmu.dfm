object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 396
  Width = 399
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files\Firebird\Firebird_3_0\examples\empbuil' +
        'd\EMPLOYEE.FDB'
      'CharacterSet=UTF8'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 296
    Top = 56
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 184
    Top = 184
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 304
    Top = 184
  end
  object quEmployee: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    employee.emp_no,'
      '    employee.first_name,'
      '    employee.last_name,'
      '    employee.salary'
      'from employee')
    Left = 40
    Top = 288
  end
  object quDepartment: TFDQuery
    AfterScroll = quDepartmentAfterScroll
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    department.dept_no,'
      '    department.department,'
      '    department.budget,'
      '    department.location,'
      '    employee.emp_no'
      'from employee'
      
        '   inner join department on (employee.dept_no = department.dept_' +
        'no)')
    Left = 152
    Top = 272
  end
end
