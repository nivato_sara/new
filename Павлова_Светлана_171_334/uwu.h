// ---------------------------------------------------------------------------

#ifndef uwuH
#define uwuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Edit.hpp>

// ---------------------------------------------------------------------------
class Tfmu : public TForm {
__published: // IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiPlay;
	TTabItem *tiSetting;
	TTabItem *tiLvl;
	TToolBar *ToolBar1;
	TToolBar *ToolBar2;
	TToolBar *ToolBar3;
	TToolBar *ToolBar4;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button5;
	TButton *Button6;
	TTabItem *tiBest;
	TToolBar *ToolBar5;
	TButton *Button4;
	TToolBar *ToolBar6;
	TButton *buPlay;
	TButton *buBest;
	TButton *buLevel;
	TToolBar *ToolBar7;
	TButton *buBlue;
	TButton *buOrange;
	TButton *buPink;
	TMemo *Memo1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TToolBar *ToolBar8;
	TButton *buRus;
	TButton *buEng;
	TLabel *labelPlayToTo;
	TToolBar *ToolBar9;
	TLabel *laSum;
	TLabel *laTime;
	TLabel *Me;
	TButton *Button8;
	TButton *Button9;
	TLabel *laQu;
	TEdit *EditTr;
	TToolBar *ToolBar10;
	TButton *buTr;
	TTimer *tm;

	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buLevelClick(TObject *Sender);
	void __fastcall buBestClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buRusClick(TObject *Sender);
	void __fastcall buEngClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buTrClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);

private: // User declarations
	void DoReset();

	TDateTime FTimerStart;

	void DoTimer();

public: // User declarations
	__fastcall Tfmu(TComponent* Owner);
	int lang = 0;

	int ranNum;
	int rnd;
	int rnd1;

	int Money = 0;
	UnicodeString WW = "";
	UnicodeString WEng = "";
	UnicodeString WRus = "";
	UnicodeString WEng1 = "";
	UnicodeString WRus1 = "";

	UnicodeString OtvRus;
	UnicodeString OtvEng;

	UnicodeString n = "�������� �����";

	String masEng[26] = {
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
		'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	String masEngTr[26] = {
		L'��', L'��', L'��', L'��', L'�', L'��', L'���', L'���', L'��', L'����',
		L'���', L'��', L'��', L'��', L'��', L'��', L'���', L'��', L'��', L'��',
		L'�', L'��', L'����-�', L'���', L'���', L'���'};
	String masRus[33] = {
		L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�',
		L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�',
		L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�', L'�'};
	String masRusTr[33] = {
		L'�', L'��', L'��', L'��', L'��', L'�', L'��', L'��', L'��', L'�',
		L'� �������', L'��', L'���', L'��', L'��', L'�', L'��', L'��', L'��',
		L'��', L'�', L'��', L'��', L'��', L'��', L'��', L'��', L'������� ����',
		L'�', L'������ ����', L'� ���������', L'�', L'��'};

};

// ---------------------------------------------------------------------------
extern PACKAGE Tfmu *fmu;
// ---------------------------------------------------------------------------
#endif
