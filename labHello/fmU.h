//---------------------------------------------------------------------------

#ifndef fmUH
#define fmUH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
//---------------------------------------------------------------------------
class TLabHello : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *Button2;
	TTabItem *TabItem2;
	TToolBar *ToolBar2;
	TButton *Button3;
	TTabItem *TabItem3;
	TToolBar *ToolBar3;
	TButton *Button5;
	TTabItem *TabItem4;
	TToolBar *ToolBar4;
	TButton *Button6;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TLabHello(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLabHello *LabHello;
//---------------------------------------------------------------------------
#endif
