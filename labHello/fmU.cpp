//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmU.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TLabHello *LabHello;
//---------------------------------------------------------------------------
__fastcall TLabHello::TLabHello(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLabHello::Button1Click(TObject *Sender)
{
	Image1->Visible = ! Image1->Visible ;
}
//---------------------------------------------------------------------------
void __fastcall TLabHello::Button2Click(TObject *Sender)
{
Image2->Visible =! Image2->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TLabHello::Button3Click(TObject *Sender)
{
Image3->Visible =! Image3->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TLabHello::Button4Click(TObject *Sender)
{
Image4->Visible =! Image4->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TLabHello::Button5Click(TObject *Sender)
{
ShowMessage("Hello, World!");
}
//---------------------------------------------------------------------------

