//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TButton *buAbout;
	TTabControl *tc;
	TTabItem *tiRestart;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *tiStart;
	TButton *buStart;
	TImage *Image1;
	TScrollBox *ScrollBox1;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TScrollBox *ScrollBox2;
	TLabel *Label3;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TScrollBox *ScrollBox3;
	TLabel *Label4;
	TButton *Button11;
	TButton *Button12;
	TButton *Button13;
	TButton *Button14;
	TButton *Button15;
	TButton *buRestart;
	TMemo *me;
	TProgressBar *pb;
	TLabel *laCountQ;
	TLabel *laCorrect;
	TLabel *laWrong;
	TImage *imCorrect;
	TImage *imWrong;
	TTabItem *TabItem1;
	TScrollBox *ScrollBox4;
	TLabel *Label5;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall Label3Click(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall Image2Click(TObject *Sender);
private:	// User declarations
int FCountCorrect;
int FCountWrong;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
